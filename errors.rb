class AssemblerSyntaxError < StandardError
end

class OutOfMemoryException < StandardError
end

class UnhandledInterruption < StandardError
end

class InvalidSyscall < StandardError
end
