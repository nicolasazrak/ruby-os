module ArchitectureConstants

  SEGMENT_DIGITS = 1
  OFFSET_DIGITS = 4 # Each segment well be in 10 pages
  FRAME_SIZE = 100 # It should be a valid n ** 10 where n < OFFSET_DIGITS

  CODE_SEGMENT = 9
  HEAP_SEGMENT = 8
  STACK_SEGMENT = 7

  def self.max_addressable_segment
    10 ** SEGMENT_DIGITS
  end

  def self.max_addressable_offset
    10 ** OFFSET_DIGITS
  end

end