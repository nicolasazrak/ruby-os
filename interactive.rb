#!/usr/bin/env ruby

require 'bundler'
require_relative 'logs.rb'
require_relative 'architecture.rb'
require_relative 'errors.rb'
require_relative 'cpu.rb'
require_relative 'ram.rb'
require_relative 'virtual_memory.rb'
require_relative 'process.rb'
require_relative 'os.rb'

Bundler.require(:default)

ram = RAM.new(size: 20_000) 
so = X86OS.new(ram)
so.load_initram_fs(File.read("compiler/ejemplos/prueba.asm"))

Pry.start(so)
