class PageFault < StandardError
  def initialize(address, segment, offset)
    super("Process does not have in memory page with address=#{address} segment=#{segment} offset=#{offset}")
    @address = address
    @segment = segment
    @offset = offset
  end
end

class OutOfVirtualSpace < StandardError
end

class SegmentationFault < StandardError
end


class Segment 
  attr_accessor :limit
  attr_accessor :pages

  def initialize
    @limit = 0
    @pages = [] # Pages where the segment is located
  end

  def check_valid_size!
    if @limit > ArchitectureConstants.max_addressable_offset
      raise OutOfVirtualSpace.new
    end
  end

  # http://lxr.linux.no/linux+v2.6.28/mm/mmap.c#L1716
  def grow(size, page_table)
    required_pages = required_pages_to_grow?(size)
    @limit += size
    check_valid_size!

    required_pages.times.each { @pages.push(page_table.request_page) } # Reserve pages
  
    @limit
  end

  def required_pages_to_grow?(size)
    last_page_used = @limit % ArchitectureConstants::FRAME_SIZE
    current_extra_space = last_page_used == 0 ? 0 : ArchitectureConstants::FRAME_SIZE - last_page_used
    
    if size == current_extra_space
      return 0
    end
    required_size = size - current_extra_space

    complete_pages = required_size / ArchitectureConstants::FRAME_SIZE
    incomplete_pages = required_size % ArchitectureConstants::FRAME_SIZE == 0 ? 0 : 1
    complete_pages + incomplete_pages
  end

  def page_for(segment_offset)
    page_position = segment_offset / ArchitectureConstants::FRAME_SIZE
    raise SegmentationFault.new("Memory offset for segment has no page") if @pages[page_position].nil?
    page_offset = segment_offset % ArchitectureConstants::FRAME_SIZE
    [@pages[page_position], page_offset]
  end
end

class VirtualMemoryProcessTable
  attr_accessor :segments
  attr_accessor :page_table

  def initialize(page_table)
    @segments = {}
    @page_table = page_table
  end

  def create_segment(segment_number:)
    @segments[segment_number] = Segment.new
  end

  def segment_numbers
    @segments.keys
  end

  def segments_size
    segment_numbers.size
  end

  def grow(segment_number:, size:)
    limit = get_segment(segment_number).grow(size, @page_table)
    segment_number * ArchitectureConstants.max_addressable_offset + limit
  end

  def get_segment(number)
    @segments[number]
  end

  def decode(address)
    segment_number = address / ArchitectureConstants.max_addressable_offset
    offset = address % ArchitectureConstants.max_addressable_offset
    segment = get_segment(segment_number)
    raise SegmentationFault.new("No such segment #{segment_number} for address=#{address}") if segment.nil?
    page, page_offset = segment.page_for(offset)
    frame = @page_table.frame_of(page)
    (frame * ArchitectureConstants::FRAME_SIZE) + page_offset
  end
end

class PageTable 

  def initialize(total_frames)
    @pages = Array.new(total_frames, true) # Pages statuses -> true=free false=used
  end

  def used_pages_count
    @pages.count { |n| !n }
  end

  def free_pages_count
    @page.count { |n| n }
  end

  def request_page
    page = @pages.find_index true
    raise OutOfMemoryException.new if page == nil
    @pages[page] = false
    page
  end

  def frame_of(page)
    # Currently as this paging algorithm does not support Swapping
    # The page and the frame are always the same
    page
  end

end

class VirtualMemory

  attr_reader :page_table
  attr_reader :process_table

  def initialize(ram)
    @ram = ram
    @logger = new_logger('vm')
    @page_table = PageTable.new(ram.total_frames) 
    @process_table = {} # pid -> VirtualMemoryProcessTable
  end

  def new_process(pid:, code_bytes:, stack_bytes:)
    raise StandardError.new("Process already exists") unless @process_table[pid].nil?

    table = VirtualMemoryProcessTable.new(@page_table)
    @process_table[pid] = table

    create_segment(pid: pid, segment_number: 1) # Stack
    create_segment(pid: pid, segment_number: 9) # Code
    create_segment(pid: pid, segment_number: 8) # Heap

    grow_segment(pid, segment_number: 1, size: stack_bytes)
    grow_segment(pid, segment_number: 9, size: code_bytes)
  end

  def create_segment(pid:, segment_number:)
    @process_table[pid].create_segment(segment_number: segment_number)
  end

  def grow_segment(pid, segment_number:, size:)
    @process_table[pid].grow(segment_number: segment_number, size: size) 
  end

  def grow_heap(pid, size)
    grow_segment(pid, segment_number: 9, size: size)
  end

  def duplicate(pid, new_pid)
    raise StandardError.new("Process already exists") unless @process_table[new_pid].nil?

    new_table = VirtualMemoryProcessTable.new(@page_table)
    @process_table[new_pid] = new_table
    original_table = @process_table[pid]

    original_table.segment_numbers.each do |segment_number|
      original_segment = original_table.get_segment(segment_number)

      create_segment(pid: new_pid, segment_number: segment_number)
      grow_segment(new_pid, segment_number: segment_number, size: original_segment.limit)

      segment_start = segment_number * ArchitectureConstants.max_addressable_offset
      original_segment.limit.times.to_a.each do |segment_offset|
        address = segment_start + segment_offset
        value = read_memory(pid, address)
        write_memory(new_pid, address, value)
      end
    end

  end

  def decode(pid, address)
    process_table = @process_table[pid]
    process_table.decode(address)
  end

  def read_memory(pid, virtual_address)
    physical_address = decode(pid, virtual_address)
    @logger.info "Read physical_address=#{physical_address} virtual_address=#{virtual_address} pid=#{pid}"    
    @ram.read_memory(physical_address)
  end

  def write_memory(pid, virtual_address, value)
    physical_address = decode(pid, virtual_address)
    @logger.info "Write value=#{value} physical_address=#{physical_address} virtual_address=#{virtual_address} pid=#{pid}"    
    @ram.write_memory(physical_address, value)
  end

end