class Registers
  GENERAL_REGISTERS = [:eax, :ebx, :ecx, :rax, :rbx, :rcx, :rdx]

  STACK_POINTER = :rsp
  STACK_BASE = :rbp
  INSTRUCTION_POINTER = :eip
  REGISTERS = GENERAL_REGISTERS + [STACK_POINTER, STACK_BASE, INSTRUCTION_POINTER]
end

class CPU
  Registers::REGISTERS.each do |r|
    attr_accessor r
  end

  attr_accessor :interruption # Interruption code
  attr_accessor :next_timer_interrupt

  def initialize(memory)
    @memory = memory
    @timer_interval = 3 # 3 seconds
    @next_timer_interrupt = Time.now + @timer_interval 
    @interruption = :"0"
    @end = false
    @cpu_logger = new_logger('cpu')
    init_registers
  end

  def init_registers
    Registers::REGISTERS.each { |register|
      self.send("#{register}=".to_sym, 0)
    }
  end

  def value_of(value)
    if value.start_with? "$"
      return value[1..-1].to_i
    end

    if value.start_with?("%") && self.respond_to?("#{value[1..-1]}".to_sym)
      return self.send value[1..-1].to_s.to_sym
    end
    
    memory_match = /\-[0-9]\(%rsp\)/.match value
    unless memory_match.nil?
      stack_offset = memory_match[0].split("(").first
      return read_memory(@rsp + stack_offset.to_i)
    end

    raise AssemblerSyntaxError.new("Syntax invalida, no se puede obtener el valor de #{value}")
  end

  def assign(position, value)
    if position.start_with?("%") && self.respond_to?("#{position[1..-1]}=".to_sym)
      return self.send "#{position[1..-1]}=".to_sym, value
    end

    memory_match = /\-[0-9]\(%rsp\)/.match(position)
    unless memory_match.nil?
      stack_offset = memory_match[0].split("(").first
      return write_memory(@rsp + stack_offset.to_i, value)
    end

    raise AssemblerSyntaxError.new("Syntax invalida, no se puede asignar en #{value}")
  end

  def add(val1, val2)
    assign(val2, value_of(val1) + value_of(val2))
  end

  def mov(val1, val2)
    assign(val2, value_of(val1))
  end

  def inc(val)
    assign(val, value_of(val) + 1)
  end

  def dec(val)
    assign(val, value_of(val) - 1)
  end

  def sum(val1, val2)
    assign(val2, value_of(val1) + value_of(val2))
  end

  def imul(val1)
    # El segundo valor es %rax
  end

  def jmp(val)
    @eip = value_of(val)
  end

  def call(position)
    push("%eip")
    @eip = value_of(position)
  end

  def push(val)
    # Push val in top of the stack
    write_memory(@rsp, value_of(val))
    @rsp += 1
  end
  
  def pop(into)
    # Write the top of the stack into val
    @rsp -= 1
    value = read_memory(@rsp)
    assign(into, value)
  end

  def shutdown
    @end = true
  end

  def noop
  end

  def jmpzero(element_to_compare, position)
    if value_of(element_to_compare) == 0
      jmp(position)
    end
  end

  def ret
    @rsp -= 1
    @eip = read_memory(@rsp)
  end

  def syscall(code)
    @interruption = :"2"
    @eax = code.to_i
  end

  def decode(asm)
    spltted = asm.split(" ")
    instruction = spltted.shift
    params = spltted.join(" ").split(",").map(&:strip)
    return instruction, params
  end

  def next_instruction_pointer
    instruction_pointer = @eip
    @eip += 1 
    instruction_pointer
  end

  def handle_timer_interrupt

  end

  def check_timer
    if Time.now > @next_timer_interrupt
      @next_timer_interrupt = Time.now + @timer_interval
      handle_timer_interrupt
    end
  end

  def handle_interruption
    # CPU just ignores interruptions. The OS is the one interested in it
    @interruption = :"0"
  end

  def do_cycle
    check_timer
    code = read_memory(next_instruction_pointer) # Use virtual memory
    @cpu_logger.info "exec=#{code}"
    instruccion, params = decode(code)
    self.send instruccion.to_sym, *params
    handle_interruption
  end

  def boot
    loop do
      do_cycle
      sleep 0.3
      # sleep
      break if @end
    end

    puts "--> CPU: Shutting down !"
  end

  def read_memory(address)
    @memory.read_memory(address)
  end

  def write_memory(address, value)
    @memory.write_memory(address, value)
  end

  def debug_info
    "stack=#{(@rsp - 10000).times.map {|i| read_memory(10000 + i) }} rsp=#{@rsp} eax=#{@eax} ebx=#{@ebx} ecx=#{@ecx} eip=#{@eip}"
  end

  def context
    Registers::REGISTERS.map { |r| [r, self.send(r)] }.to_h
  end

  def context=(current_context)
    current_context.each do |register, value|
      self.send("#{register}=", value)
    end
  end

end
