class PCB

  attr_accessor :pid
  attr_accessor :parent
  attr_accessor :context

  def initialize(pid, parent, context)
    @pid = pid
    @parent = parent
    @context = context
  end

end

class ProcessTable

  def initialize
    @id_counter = 1
    @procceses = {}
  end

  def new_process(parent, from_context)
    process = PCB.new(@id_counter, parent, from_context)
    @procceses[process.pid] = process
    @id_counter += 1
    process
  end

  def reschedule
    @procceses.values.sample
  end

  def save_context(pid, context)
    @procceses[pid].context = context
  end

  def process_count
    @procceses.size
  end

  def find_pid(pid)
    @procceses[pid]
  end

end