# Ruby OS

## Requirements

- Ruby > 2.4

## Usage

**Install**

First run `bundle install` to fetch dependencies

**Compiler** 

Run `./compiler.rb ejemplos/prueba.ansisop ejemplos/prueba.asm` to compile the example

**Simulator**

Start interactive mode with `DEBUG=* ./interactive.rb` and boot the operating system with `boot` method inside pry

**Tests**

`bundler exec rspec --format documentation`

## Usefull links

- Assembler introduction: https://www3.nd.edu/~dthain/courses/cse40243/fall2015/intel-intro.html
- Assembler calling conventions: https://stackoverflow.com/a/2538212/2913715
- Paging and segmentation: https://es.slideshare.net/prochwani95/paging-and-segmentationa005-a006
- Process memory: http://duartes.org/gustavo/blog/post/anatomy-of-a-program-in-memory/
- Kernel memory structures: http://duartes.org/gustavo/blog/post/how-the-kernel-manages-your-memory/
- Segmentation and pagination: http://duartes.org/gustavo/blog/post/memory-translation-and-segmentation/
