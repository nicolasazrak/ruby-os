class RAM

  attr_reader :total_frames
  attr_reader :frame_size

  def initialize(size:)
    if (size % ArchitectureConstants::FRAME_SIZE) != 0
      raise StandardError.new("Memory size should be a multiple of frame size")
    end

    @logger = new_logger("ram")
    @total_frames = size / ArchitectureConstants::FRAME_SIZE
    @frame_size = ArchitectureConstants::FRAME_SIZE
    @ram_space = Array.new(size, 0)
  end

  def read_memory(address)
    # @logger.info "Reading address=#{address}"
    @ram_space[address]
  end

  def write_memory(address, value)
    # @logger.info "Write address=#{address}"
    @ram_space[address] = value
  end

  def read_full_page(frame_number)
    from = @frame_size * frame_number
    to = from + @frame_size
    # @logger.info "Read page from=#{from} to=#{to}"
    @ram_space[from..to]
  end

end