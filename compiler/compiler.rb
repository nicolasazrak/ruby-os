#!/usr/bin/env ruby
require 'pry'
require 'colorize'
load 'parser.rb'
load 'symbols.rb'
load 'parser_blocks.rb'
load '../architecture.rb'


class Compiler
  include Parser

  @parameters_calling_convention = ["%eax", "%ebx", "%ecx", "%edx", "%eex"]
  class << self
    attr_accessor :parameters_calling_convention
  end

  def generate(code)
    symbol_table = SymbolTable.new(nil)
    block_address = ArchitectureConstants::CODE_SEGMENT * ArchitectureConstants.max_addressable_offset

    ast = parse(code)
    ast.bind_symbols(symbol_table)
    ast.validate
    asm_blocks = ast.generate
    
    asm_blocks.each { |b|
        b.address = block_address
        block_address += 1
        b
      }
      .map { |b| b.fill_missing_references }
      .join("\n") 
  end

  def compile(from, to)
    input = File.read(from)
    generated = generate(input)
    File.open(to, File::RDWR|File::CREAT, 0644) {|f|
      f.rewind
      f.write(generated)
      f.flush
      f.truncate(f.pos)
    }
  end

end

if __FILE__ == $0
  if ARGV.size < 1
    puts "Invalid usage. Use <source> [<destination>]"
    exit 1
  end

  source = ARGV[0]
  dest = ARGV[1] || (source.gsub(".ansisop", "") + ".asm")

  begin
    Compiler.new.compile(source, dest)
  rescue UserError, InvalidSyntax => e
    puts e.message.colorize(:red)
  end
    
end