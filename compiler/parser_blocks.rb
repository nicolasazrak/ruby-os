class NotAssignableError < UserError
  def initialize(line, value)
    @line = line
    @value = value
  end

  def message
    "Syntax error -> '#{@line}', #{@value} is not assignable"
  end
  
  def to_s
    message
  end

end

class GeneratedBlock
  attr_accessor :asm
  attr_accessor :address
  attr_accessor :comment
  attr_accessor :orignal_block

  def initialize(asm, orignal_block, comment=nil)
    @asm = asm
    @orignal_block = orignal_block
    @comment = comment
  end

  def fill_missing_references
    solved = @asm

    # Replace the position of functions
    missing_reference = /\[.+\]/.match(solved)
    unless missing_reference.nil?
      missing_reference = missing_reference[0][1..-2]
      reference = @orignal_block.symbol_table.get_symbol_by_name(missing_reference)
      solved = solved.sub("[#{missing_reference}]", "$#{reference.address}")
    end

    # Add comments
    solved = solved.ljust(40, ' ') + "; #{@comment}"
    solved = solved.ljust(80, ' ') + " #{@address}"
    solved
  end
end

=begin
Basic parser Block which represents a sentence
which will be translated to some ASM instructions 
=end
class ParserBlock
  attr_accessor :line
  attr_accessor :symbol_table

  def initialize(line)
    @line = line
  end

  def new_variable(name)
    VariableIdentifier.new(name, line: @line)
  end

  def new_argument(name)
    ArgumentIdentifier.new(name, line: @line)
  end

  def new_label(name)
    LabelIdentifier.new(name, line: @line)
  end

  def new_function(name)
    FunctionIdentifier.new(name, line: @line)
  end

  def assert_not_constant!(value)
    if !Parser.valid_variable_name?(value) && !Parser.valid_argument_name?(value)
      raise NotAssignableError.new(@line, value)
    end
  end

  def generate_var(value)
    if Parser.numeric?(value)
      return "$#{value}"
    end

    symbol = @symbol_table.get_symbol_by_name(value)
    return "#{symbol.address}(%rsp)"
  end

  def as_generated_asm(blocks)
    blocks.map { |b| GeneratedBlock.new(b, self) }
  end
end

#
# Supported Language Blocks
#

class ProgramBlock < ParserBlock

  def initialize(blocks)
    @blocks = blocks
  end

  def bind_symbols(symbol_table)
    @blocks.each { |b| b.bind_symbols(symbol_table) }
  end

  def validate
    @blocks.each { |b| b.validate }
  end

  def generate
    @blocks.map { |b| b.generate }.flatten(1)
  end

end

class FunctionDefinitionBlock < ParserBlock
  attr_accessor :blocks
  attr_accessor :name
  attr_accessor :global_scope
  attr_accessor :local_scope

  def initialize(line, blocks)
    @line = line
    @blocks = blocks
    if !@blocks.last.is_a? ReturnBlock
     @blocks << ReturnBlock.new("return 0") # Implicit return value of 0
    end
  end

  # Helpers

  def name
    line.split(" ").last
  end

  # Pre process

  def bind_symbols(symbol_table)
    @global_scope = symbol_table
    @local_scope = symbol_table.new_scope
    @global_scope.add(new_function(name))
    @blocks.each { |b| b.bind_symbols(@local_scope) }
  end

  # validate

  def validate
    @blocks.each { |b| b.validate }
  end

  # Code generation

  def generate_debug_info
    [GeneratedBlock.new("noop", self, @line)]
  end

  def generate_push_parameters
    @local_scope.arguments.each_with_index.map do |p, i|
      register = Compiler.parameters_calling_convention[i]
      GeneratedBlock.new("push #{register}", self, "Move argument $#{i} to stack")
    end
  end

  def generate_push_variables
    [GeneratedBlock.new("add $#{@local_scope.variables.size}, %rsp", self, "Push new variables")]
  end

  def generate_inner_code
    @blocks.map { |block| block.generate }.select { |code| !code.empty? }.flatten(1)
  end

  def generate
    @local_scope.allocate_stack

    debug_info_generated_block = generate_debug_info()
    @global_scope.set_symbol_generated_block(new_function(name), debug_info_generated_block[0])

    [
      debug_info_generated_block, 
      generate_push_parameters,
      generate_push_variables,
      generate_inner_code,
    ].flatten(1)
  end

end

class VariableDefinitionBlock < ParserBlock
  def bind_symbols(symbol_table)
    @line
      .sub("variables", "")
      .strip
      .split(",")
      .each { |name| 
        if Parser.valid_argument_name? name
          symbol_table.add(new_argument(name.strip))
        else
          symbol_table.add(new_variable(name.strip))
        end
      }
  end

  def validate
  end

  def generate
    []
  end
end

class SyscallBlock < ParserBlock
  def syscall_number
    @line.split(" ").last.strip
  end

  def bind_symbols(symbol_table)
  end

  def validate
  end

  def generate
    [GeneratedBlock.new("syscall #{syscall_number}", self)]
  end
end

class AssignBlock < ParserBlock

  # Helpers

  def left_side
    line.split("=").first.strip
  end

  def right_side
    line.split("=").last.strip
  end

  # bind_symbols

  def bind_symbols(symbol_table)
    @symbol_table = symbol_table
  end

  # validate

  def validate
    assert_not_constant!(left_side)

    if Parser.valid_variable_name?(left_side)
      symbol_table.ensure_exists! new_variable(left_side)
    end

    if Parser.valid_argument_name?(left_side)
      symbol_table.ensure_exists! new_argument(left_side)
    end

    if Parser.valid_variable_name?(right_side)
      symbol_table.ensure_exists! new_variable(right_side) 
    end

    if Parser.valid_argument_name?(right_side)
      symbol_table.ensure_exists! new_argument(right_side)
    end
  end

  # generate

  def generate
    left = generate_var(right_side)
    right = generate_var(left_side)
    asm = "mov #{left}, #{right}"
    comment = "Assign #{left_side} = #{right_side}"
    [GeneratedBlock.new(asm, self, comment)]
  end

end

class FunctionCallBlock < ParserBlock

  # Helpers

  def function_name
    @line.split("<-").last.strip.split("(").first
  end

  def parameters_string
    parameters = @line.split("(").last.split(")")
    return [] if parameters.empty?
    parameters.first.split(",").map(&:strip)
  end

  def parameters_symbols
    parameters_string.map do |p|
      if Parser.valid_argument_name? p
        new_argument(p)
      elsif Parser.valid_variable_name? p
        new_variable(p)
      else
        raise InvalidSyntax.new(@line, "Invalid parameter '#{p}'")
      end
    end
  end

  def has_assignment?
    @line.include?("<-")
  end

  def left_side
    @line.split("<-").first.strip
  end

  # bind_symbols

  def bind_symbols(symbol_table)
    @symbol_table = symbol_table
    parameters_symbols.each do |p| 
      symbol_table.ensure_exists!(p)
    end

    if has_assignment?
      assert_not_constant!(left_side)

      if Parser.valid_argument_name?(left_side)
        symbol_table.ensure_exists!(new_argument(left_side))
      end

      if Parser.valid_variable_name?(left_side)
        symbol_table.ensure_exists!(new_variable(left_side))
      end
    end
  end

  # validate

  def validate
    @symbol_table.ensure_exists! new_function(function_name)
  end

  # generation

  def generate_parameters
    parameters_string.each_with_index.map do |p, i| 
      left = generate_var(p)
      right = Compiler.parameters_calling_convention[i]
      comment = "Copy parameter #{p}"
      GeneratedBlock.new("mov #{left}, #{right}", self, comment)
    end
  end

  def generate_call
    [GeneratedBlock.new("call [#{function_name}]", self, @line)]
  end

  def generate_assignment
    return [] if !has_assignment?

    message = "Assign return value to #{left_side}"
    return [GeneratedBlock.new("mov %eax, #{generate_var(left_side)}", self, message)]
  end

  def generate
    generate_parameters + generate_call + generate_assignment
  end
end

class ReturnBlock < ParserBlock
  def return_value
    @line.split("return ").last
  end

  def bind_symbols(symbol_table)
    @symbol_table = symbol_table
    if Parser.valid_argument_name?(return_value)
      symbol_table.ensure_exists! new_argument(return_value)
      return
    end

    if Parser.valid_variable_name?(return_value)
      symbol_table.ensure_exists! new_variable(return_value)
      return
    end

    if Parser.numeric?(return_value)
      return
    end

    raise InvalidSyntax.new(@line, "Invalid return value")
  end

  def validate
  end

  # Generate 

  def generate_pop_variables
    [GeneratedBlock.new("add $-#{@symbol_table.variables.size}, %rsp", self, "Pop variables")]
  end

  def generate_pop_parameters
    @symbol_table.arguments.each_with_index.map do |p, i|
      register = Compiler.parameters_calling_convention[i]
      GeneratedBlock.new("pop #{register}", self, "Remove argument $#{i} from stack")
    end
  end

  def generate_returns_value
    [
      GeneratedBlock.new("mov #{generate_var(return_value)}, %eax", self),
      GeneratedBlock.new("ret", self),
      GeneratedBlock.new("noop", self, "---end function")
    ]
  end

  def generate
    generate_pop_variables + generate_pop_parameters + generate_returns_value
  end

end

class ASMBlock < ParserBlock
  def asm
    @line.sub("ASM ", "")
  end

  def bind_symbols(symbol_table)
  end

  def validate
  end

  def generate
    [GeneratedBlock.new(asm, self)]
  end
end

class LabelDefinitionBlock < ParserBlock
  def name
    @line.split(":").first
  end

  def bind_symbols(symbol_table)
    @symbol_table = symbol_table
    symbol_table.add new_label(name)
  end

  def validate
  end

  def generate
    noop = GeneratedBlock.new("noop", self, "Label: #{name}")
    @symbol_table.set_symbol_generated_block(new_label(name), noop)
    [noop]
  end
end

class JumpBlock < ParserBlock
  def label_name
    @line.split(" ").last
  end

  def bind_symbols(symbol_table)
    @symbol_table = symbol_table
  end

  def validate
    @symbol_table.ensure_exists! new_label(label_name)
  end

  def generate
    [GeneratedBlock.new("jmp [#{label_name}]", self, @line)]
  end
end

class JumpZeroBlock < ParserBlock
  def label_name
    @line.split(" ").last
  end

  def element_to_compare
    @line.split(" ")[1]
  end

  def bind_symbols(symbol_table)
    @symbol_table = symbol_table

    if Parser.valid_variable_name?(element_to_compare)
      @symbol_table.ensure_exists! new_variable(element_to_compare)
      return
    end

    if Parser.valid_argument_name?(element_to_compare)
      @symbol_table.ensure_exists! new_argument(element_to_compare)
      return
    end

    if Parser.numeric?
      return
    end

    raise InvalidSyntax.new(@line, "Could not evaluate #{element_to_compare}")
  end

  def validate
    @symbol_table.ensure_exists! new_label(label_name)
  end

  def generate
    [GeneratedBlock.new("jmpzero #{generate_var(element_to_compare)}, [#{label_name}]", self, @line)]
  end
end