class Identifier 
  attr_accessor :name
  attr_accessor :address
  attr_accessor :line

  def initialize(name, line: :line)
    @name = name
    @line = line
  end

end

class VariableIdentifier < Identifier
  attr_accessor :address

  def goes_in_stack?
    true
  end

  def allocation_priority
    100
  end

  def type_s
    "variable"
  end
end

class ArgumentIdentifier < Identifier
  attr_accessor :address

  def goes_in_stack?
    true
  end

  def type_s
    "argument"
  end

  def position
    name.gsub("$", "")
  end

  def allocation_priority
    position.to_i
  end
end

class FunctionIdentifier < Identifier
  attr_accessor :block

  def address
    @block.address
  end

  def type_s
    "function"
  end

  def goes_in_stack?
    false
  end
end

class LabelIdentifier < Identifier
  attr_accessor :block

  def address
    @block.address
  end

  def type_s
    "label"
  end

  def goes_in_stack?
    false
  end
end



class SymbolNotFound < StandardError
end

class UserError < StandardError
end

class DuplicatedSymbol < UserError
  def initialize(new_symbol, previous_symbol)
    @new_symbol = new_symbol
    @previous_symbol = previous_symbol
  end

  def message
    "--> '#{@new_symbol.line}' is trying to register a new #{@new_symbol.type}, but it was already declared in '#{@previous_symbol.line}'"
  end

  def to_s
    message
  end
end

class MissingSymbol < UserError
  def initialize(searched_symbol)
    @searched_symbol = searched_symbol
  end

  def message
    "--> Unknown #{@searched_symbol.type_s} '#{@searched_symbol.name}', it was used but not declared. Line: '#{@searched_symbol.line}'" 
  end

  def to_s
    message
  end
end

class SymbolTable

  attr_accessor :symbols

  def initialize(parent_scope)
    @pending_checks = []
    @symbols = {}
    @parent_scope = parent_scope
  end

  def new_scope
    SymbolTable.new self
  end

  def add(new_symbol)
    if @symbols.key? new_symbol.name
      raise DuplicatedSymbol.new(new_symbol, @symbols[new_symbol.name])
    end
    @symbols[new_symbol.name] = new_symbol
  end

  # This is just an alias
  def ensure_exists!(searched_symbol)
    get_symbol(searched_symbol) 
    nil
  end

  def get_symbol(searched_symbol)
    symbol = @symbols[searched_symbol.name] 
    
    if !symbol.nil? && symbol.is_a?(searched_symbol.class)
      return symbol
    end

    if @parent_scope.nil?
      raise MissingSymbol.new(searched_symbol) if symbol.nil?
    end

    @parent_scope.get_symbol(searched_symbol)
  end

  def get_symbol_by_name(name)
    symbol = @symbols[name]
    return symbol unless symbol.nil?

    if @parent_scope.nil?
      raise SymbolNotFound.new("Could not found symbol #{name}. This is probably a compiler bug, it should be checked first.")
    end

    @parent_scope.get_symbol_by_name(name)
  end

  def set_symbol_generated_block(searched_symbol, generated_block)
    symbol = get_symbol(searched_symbol)
    symbol.block = generated_block
  end

  def arguments_count
    arguments.count 
  end

  def arguments
    @symbols.values.select { |s| s.is_a? ArgumentIdentifier }.sort_by { |s| s.name }
  end

  def variables
    @symbols.values.select { |s| s.is_a? VariableIdentifier }
  end

  def allocate_stack
    stack_position = -1
    @symbols.values.select { |s| s.goes_in_stack? }.sort_by { |s| s.allocation_priority }.reverse.each do |identifier|
      identifier.address = stack_position
      stack_position -= 1
    end
  end
end
