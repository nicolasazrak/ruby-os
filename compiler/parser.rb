class InvalidSyntax < StandardError

  def initialize(line, extra_message='')
    @line = line
    @extra_message = extra_message
  end

  def message
    "Invalid syntax at '#{@line}'. #{@extra_message}\n                   ↑"
  end

  def to_s
    message
  end

end

module Parser

  def self.numeric?(lookAhead)
    lookAhead =~ /^[0-9]$/
  end

  def self.valid_variable_name?(lookAhead)
    lookAhead =~ /^[A-Za-z]$/
  end

  def self.valid_argument_name?(lookAhead)
    lookAhead =~ /\$[0-9]$/
  end

  def is_function_definition?(line)
    line =~ /^function\s+[a-zA-Z_][a-zA-Z0-9]+$/
  end

  def is_variable_definition?(line)
    line.start_with?("variables ") && line.split("variables ").last.split(",").all? do |v| 
      Parser.valid_variable_name?(v.strip) || Parser.valid_argument_name?(v.strip)
    end
  end

  def is_assign?(line)
    line.include? "="
  end

  def is_function_call?(line)
    line.include?("(") && line.include?(")")
  end

  def is_asm?(line)
    line.start_with?("ASM ")
  end

  def is_block_end?(line)
    line == "end"
  end

  def is_syscall?(line)
    line =~ /^syscall [0-9]+$/
  end

  def is_jump?(line)
    line =~ /^jmp [a-zA-Z][a-zA-Z0-9]+$/
  end

  def is_jump_zero?(line)
    line =~ /^jmpzero /
  end

  def is_label_definition?(line)
    line =~ /^[a-zA-Z][a-zA-Z0-9]+\:$/
  end

  def clean_line(line)
    if line.empty?
      return ""
    end
    line.split("//").first.strip # Clean up comments
  end

  def is_return?(line)
    line.start_with?("return ")
  end

  def parse(source)
    lib_ansisop = "
    function _main
      variables e
      e <- main()
      exit(e)
    end

    function print
      ASM mov %eax, %ecx 
      syscall 1
    end

    function suma
      ASM sum %ebx, %eax
      ASM ret
    end

    function exit
      ASM mov %eax, %ebx
      syscall 2 
    end

    function fork
      syscall 3
      ASM ret
    end
    "
    source = lib_ansisop + source
    lines = source.strip.split("\n")

    functions = []
    current_function_line = nil
    current_function_blocks = nil
    inside_function = false

    lines.each do |line|
      line = clean_line(line)
      if line.empty?
        next
      end

      if !inside_function && !is_function_definition?(line)
        raise StandardError.new ("Invalid line: #{line}. Everything should be inside a function")
      end

      if is_block_end?(line)
        block = FunctionDefinitionBlock.new(current_function_line, current_function_blocks)
        functions << block
        inside_function = false
        next
      end

      if is_function_definition?(line)
        current_function_line = line
        current_function_blocks = []
        inside_function = true
        next
      end
      
      if is_variable_definition?(line)
        block = VariableDefinitionBlock.new line
        current_function_blocks.push block
        next
      end

      if is_assign?(line)
        block = AssignBlock.new line
        current_function_blocks.push block
        next
      end

      if is_syscall?(line)
        block = SyscallBlock.new line
        current_function_blocks.push block
        next
      end

      if is_asm?(line)
        block = ASMBlock.new line
        current_function_blocks.push block
        next
      end

      if is_function_call?(line)
        block = FunctionCallBlock.new line
        current_function_blocks.push block
        next
      end

      if is_label_definition?(line)
        block = LabelDefinitionBlock.new line
        current_function_blocks.push block
        next
      end
      
      if is_jump?(line)
        block = JumpBlock.new line
        current_function_blocks.push block
        next
      end

      if is_jump_zero?(line)
        block = JumpZeroBlock.new line
        current_function_blocks.push block
        next
      end

      if is_return?(line)
        block = ReturnBlock.new line
        current_function_blocks.push block
        next
      end
      
      raise InvalidSyntax.new line
    end

    ProgramBlock.new(functions)
  end

end