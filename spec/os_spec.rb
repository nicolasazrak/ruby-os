require 'rspec'
require_relative '../logs.rb'
require_relative '../errors.rb'
require_relative '../architecture.rb'
require_relative '../virtual_memory.rb'
require_relative '../ram.rb'
require_relative '../cpu.rb'
require_relative '../process.rb'
require_relative '../os.rb'

RSpec.describe X86OS do

  before :each do
    @ram = RAM.new(size: 400)
    @os = X86OS.new @ram
  end

  context "#fork" do

    it "should create a new process" do
      @os.load_initram_fs("")
      expect(@os.procceses.process_count).to eq(1)
      expect(@os.syscall_fork).to eq(2)
      expect(@os.procceses.process_count).to eq(2)
    end

    it "should be able to handle different memory contexts" do
      @os.load_initram_fs("")
      @os.write_memory(10001, 6)
      expect(@os.read_memory(10001)).to eq(6)

      @os.syscall_fork

      @os.do_context_switch(@os.procceses.find_pid(2))
      @os.write_memory(10001, 3)
      expect(@os.read_memory(10001)).to eq(3)

      @os.do_context_switch(@os.procceses.find_pid(1))
      expect(@os.read_memory(10001)).to eq(6)
    end
  end

end