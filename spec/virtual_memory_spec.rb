require 'rspec'
require_relative '../logs.rb'
require_relative '../errors.rb'
require_relative '../architecture.rb'
require_relative '../virtual_memory.rb'
require_relative '../ram.rb'


RSpec.describe Segment do

  before :each do
    @page_table = PageTable.new(200)
  end

  context "#required_pages_to_grow?" do
    it "should work when segment is empty" do
      segment = Segment.new
      expect(segment.required_pages_to_grow?(20)).to eq(1)
      expect(segment.required_pages_to_grow?(120)).to eq(2)
      expect(segment.required_pages_to_grow?(100)).to eq(1)
      expect(segment.required_pages_to_grow?(300)).to eq(3)
    end

    it "should work when segment has a full page" do
      segment = Segment.new
      segment.limit = 100
      expect(segment.required_pages_to_grow?(20)).to eq(1)
      expect(segment.required_pages_to_grow?(120)).to eq(2)
    end

    it "should work with remaining space" do
      segment = Segment.new
      segment.limit = 120
      expect(segment.required_pages_to_grow?(20)).to eq(0)
      expect(segment.required_pages_to_grow?(80)).to eq(0)
      expect(segment.required_pages_to_grow?(100)).to eq(1)
    end
  end

  context "#page" do
    it "should work on position 0" do
      segment = Segment.new
      segment.limit = 300
      segment.pages = [5, 9, 25]
      expect(segment.page_for(0)).to eq([5, 0])
      expect(segment.page_for(100)).to eq([9, 0])
      expect(segment.page_for(200)).to eq([25, 0])
    end

    it "should work on last position" do
      segment = Segment.new
      segment.limit = 300
      segment.pages = [5, 9, 25]
      expect(segment.page_for(99)).to eq([5, 99])
      expect(segment.page_for(199)).to eq([9, 99])
      expect(segment.page_for(299)).to eq([25, 99])
    end

    it "should work on middle positions" do
      segment = Segment.new
      segment.limit = 235
      segment.pages = [5, 9, 25]
      expect(segment.page_for(3)).to eq([5, 3])
    end

    it "should throw segmentation fault if it does not have a page" do
      segment = Segment.new
      segment.limit = 100
      segment.pages = [5]
      expect { segment.page_for(100) }.to raise_error(SegmentationFault)
    end

  end

end


RSpec.describe VirtualMemoryProcessTable do 

  before :each do
    @page_table = PageTable.new 200
    @table = VirtualMemoryProcessTable.new(@page_table)
  end

  it "should be able to create new segments" do
    @table.create_segment(segment_number: 0)
    expect(@table.segments_size).to eq(1)
  end

  it "should be able to grow a segment" do
    @table.create_segment(segment_number: 0)              # Stack starts at 0
    @table.grow(segment_number: 0, size: 20)              # It grows 20 bytes
    @table.grow(segment_number: 0, size: 30)              # It grows 30 bytes
    expect(@table.get_segment(0).limit).to eq(50)         # Its limit should be 50
  end

  it "should check max size when growing" do
    @table.create_segment(segment_number: 0) 
    @table.grow(segment_number: 0, size: 5000)
    @table.grow(segment_number: 0, size: 5000)
    expect { @table.grow(segment_number: 0, size: 1) }.to raise_error(OutOfVirtualSpace)
  end

  it "should reserve pages when the segment grows" do
    @table.create_segment(segment_number: 0)
    expect(@page_table.used_pages_count).to eq(0)
 
    @table.grow(segment_number: 0, size: 20)
    expect(@page_table.used_pages_count).to eq(1)
 
    @table.grow(segment_number: 0, size: 60)
    expect(@page_table.used_pages_count).to eq(1)
 
    @table.grow(segment_number: 0, size: 160)
    expect(@page_table.used_pages_count).to eq(3)
  end

  it "should reserve pages when the are different segments and not reuse them" do
    @table.create_segment(segment_number: 0)
    @table.create_segment(segment_number: 1)
    expect(@page_table.used_pages_count).to eq(0)
 
    @table.grow(segment_number: 0, size: 20)
    expect(@page_table.used_pages_count).to eq(1)
 
    @table.grow(segment_number: 1, size: 60)
    expect(@page_table.used_pages_count).to eq(2)
 
    @table.grow(segment_number: 0, size: 20)
    expect(@page_table.used_pages_count).to eq(2)
    
    @table.grow(segment_number: 1, size: 60)
    expect(@page_table.used_pages_count).to eq(3)
  end

  context "#decode" do
    it "should throw segmentation fault if segment does not exists" do 
      expect { @table.decode(50000) }.to raise_error(SegmentationFault)
    end

    it "should calculate the physical address" do
      @table.create_segment(segment_number: 3)
      @table.create_segment(segment_number: 5)
      @table.create_segment(segment_number: 8)
      @table.grow(segment_number: 3, size: 500)
      @table.grow(segment_number: 5, size: 300)
      @table.grow(segment_number: 8, size: 200)

      expect(@table.decode(30199)).to eq(199)
      expect(@table.decode(50123)).to eq(623)
      expect(@table.decode(80099)).to eq(899)

      @table.grow(segment_number: 3, size: 100)
      expect(@table.decode(30529)).to eq(1029)
    end
  end

end


RSpec.describe VirtualMemory do

  before :each do
    @ram = RAM.new(size: 400)
    @vm = VirtualMemory.new(@ram)
  end

  context "#duplicate" do
    it "should fail if process table already exists" do
      @vm.new_process(pid: 1, code_bytes: 2, stack_bytes: 3)
      expect { @vm.duplicate(1, 1) }.to raise_error(StandardError)
    end

    it "should create a new process table" do
      @vm.new_process(pid: 1, code_bytes: 2, stack_bytes: 3)
      @vm.duplicate(1, 2)
      expect(@vm.process_table.keys).to eq([1, 2])
    end

    it "should create the same segments" do
      @vm.new_process(pid: 1, code_bytes: 2, stack_bytes: 3)
      
      @vm.create_segment(pid: 1, segment_number: 4)
      @vm.duplicate(1, 2)

      first_process_segments = @vm.process_table[1].segment_numbers
      new_process_segments = @vm.process_table[2].segment_numbers
      
      expect(new_process_segments).to eq(first_process_segments)
    end

    it "should reserve the same quantity of pages" do
      @vm.new_process(pid: 1, code_bytes: 2, stack_bytes: 3)
      @vm.create_segment(pid: 1, segment_number: 4)
      used_pages = @vm.page_table.used_pages_count
      @vm.duplicate(1, 2)

      expect(@vm.page_table.used_pages_count).to eq(used_pages * 2)
    end

    it "should copy every content" do
      @vm.new_process(pid: 1, code_bytes: 2, stack_bytes: 3)
      @vm.grow_segment(1, segment_number: 1, size: 10)
      @vm.write_memory(1, 10000, 10)
      @vm.write_memory(1, 10001, 50)
      @vm.write_memory(1, 10002, 11)

      @vm.duplicate(1, 2)

      expect(@vm.read_memory(1, 10000)).to eq(10)
      expect(@vm.read_memory(1, 10001)).to eq(50)
      expect(@vm.read_memory(1, 10002)).to eq(11)
    end

    it "should handle different contexts" do
      @vm.new_process(pid: 1, code_bytes: 20, stack_bytes: 30)      
      @vm.duplicate(1, 2)

      @vm.write_memory(1, 10003, 7)
      expect(@vm.read_memory(1, 10003)).to eq(7)
      
      @vm.write_memory(2, 10003, 9)

      expect(@vm.read_memory(1, 10003)).to eq(7)
      expect(@vm.read_memory(2, 10003)).to eq(9)
    end
  end
end