require 'logger'

def new_logger(name)
  logger = Logger.new(STDOUT)

  logger.formatter = proc do |severity, datetime, progname, msg|
    "#{name.upcase.ljust(7, ' ')} --> #{msg}\n"
  end
  
  debug_env = ENV["DEBUG"] || ""
  debug_modules = debug_env.split(",").map(&:downcase)

  if debug_modules.include?(name.downcase) || (debug_env == "*") then
    logger.level = Logger::DEBUG
  else
    logger.level = Logger::ERROR
  end

  logger
end