class X86OS < CPU

  attr_accessor :current_pid # Current process id
  attr_reader :memory # Just for testing
  attr_reader :procceses

  def initialize(memory)
    super
    @memory = VirtualMemory.new(memory)
    @procceses = ProcessTable.new
    @logger = new_logger('os')
    @process_logger = new_logger('process')
    @syscall_logger = new_logger('syscall')
  end

  def interruptions_vector
    {
      :"2" => :syscall_handler
    }
  end

  def syscalls_handlers
    {
      :"1" => :syscall_fwrite,
      :"2" => :syscall_sbrk,
      :"3" => :syscall_fork,
    }
  end

  def handle_interruption
    if @interruption != :"0"
      handler = interruptions_vector[@interruption]
      raise UnhandledInterruption.new if handler == nil
      self.send handler
      @interruption = :"0"
    end
  end

  def handle_timer_interrupt
    next_process = @procceses.reschedule
    if next_process.pid != @current_pid
      do_context_switch(next_process)
    end
  end

  def do_context_switch(next_process)
    @logger.info("Context switch OLD_PID->#{@current_pid} NEW_PID->#{next_process.pid}")
    @procceses.save_context(@current_pid, context)
    self.context= next_process.context
    @current_pid = next_process.pid
  end

  def syscall_handler
    handler = syscalls_handlers[@eax.to_s.to_sym]
    raise InvalidSyscall.new if handler == nil
    return_value = self.send handler
    @eax = return_value
  end

  def syscall_fwrite
    @process_logger.info "PID=#{current_pid} OUTPUT=#{@ecx}"
    @ecx # To prevent returning a bad value
  end

  def syscall_sbrk
    next_position = memory.grow_heap(@current_pid, @ebx)
    @syscall_logger.info "SBRK, process now has: #{memory.process(@current_pid).keys.size} assigned pages"
    next_position
  end

  def syscall_fork
    @eax = 0 # To save it in the old process    
    new_process = @procceses.new_process(@current_pid, context)
    @memory.duplicate(@current_pid, new_process.pid)
    @syscall_logger.info "FORK new PID #{new_process.pid}"
    new_process.pid
  end

  def load_initram_fs(code)
    init_process = @procceses.new_process(nil, context)
    @current_pid = init_process.pid
    address = 9 * ArchitectureConstants.max_addressable_offset
    processed_code = code.strip.split("\n")
    @memory.new_process(pid: @current_pid, code_bytes: processed_code.size, stack_bytes: 50)
    @eip = 90000
    @rsp = 10000

    processed_code.each do |instruction|
      write_memory(address, instruction.split(";").first.strip)
      address += 1
    end
  end

  def read_memory(address)
    @memory.read_memory(@current_pid, address)
  end

  def write_memory(address, value)
    @memory.write_memory(@current_pid, address, value)
  end

end